package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.stream.Collectors;

public class ParkParsingServiceImpl implements ParkParsingService {
    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        HashMap<String, String> parsedParkFields = null;
        try {
            parsedParkFields = parseFile(parkDatafilePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Class<Park> parkClass = Park.class;
        ArrayList<ParkParsingException.ParkValidationError> errors = new ArrayList<>();

        HashMap<String, String> finalParsedParkFields = parsedParkFields;
        Park park = null;
        try {
            Constructor<Park> declaredConstructor = parkClass.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);

            try {
                park = declaredConstructor.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        Park finalPark = park;
        Arrays.stream(parkClass.getDeclaredFields())
                .forEach(field -> {
                    boolean hasError = false;
                    field.setAccessible(true);
                    if (field.getAnnotation(NotBlank.class) != null) {
                        String fieldValue = finalParsedParkFields.get(field.getName());
                        if (fieldValue == null || fieldValue.equals("")) {
                            errors.add(new ParkParsingException.ParkValidationError(field.getName(),
                                    "поле не должно быть пустым или быть null"));
                            hasError = true;
                        }
                    }
                    if (field.getAnnotation(FieldName.class) != null) {
                        FieldName annotation = field.getAnnotation(FieldName.class);
                        if (finalParsedParkFields.containsKey(annotation.value())) {
                            finalParsedParkFields.replace(field.getName(), finalParsedParkFields.get(annotation.value()));
                        } else {
                            errors.add(new ParkParsingException.ParkValidationError(field.getName(),
                                    "не найдено поле, из которого нужно брать значение"));
                            hasError = true;
                        }
                    }
                    if (field.getAnnotation(MaxLength.class) != null) {
                        MaxLength annotation = field.getAnnotation(MaxLength.class);
                        if (finalParsedParkFields.get(field.getName()).length() > annotation.value()) {
                            errors.add(new ParkParsingException.ParkValidationError(field.getName(),
                                    "длина поля превышена"));
                            hasError = true;
                        }
                    }
                    if (!hasError) {
                        if (field.getType().equals(String.class)) {
                            try {
                                field.set(finalPark, finalParsedParkFields.get(field.getName()));
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                        if (field.getType().equals(LocalDate.class)) {
                            LocalDate localDate = LocalDate.parse(finalParsedParkFields.get(field.getName()));
                            try {
                                field.set(finalPark, localDate);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
        if (!errors.isEmpty()) {
            throw new ParkParsingException("Найдены ошибки при парсинге файла!", errors);
        }
        return park;
    }

    private HashMap<String, String> parseFile(String parkDataFilePath) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(parkDataFilePath));
        HashMap<String, String> parkFields = new HashMap<>();
        String str;
        reader.readLine();
        while (!(str = reader.readLine()).equals("***")) {

            List<String> collect = Arrays.stream(str.split(":"))
                    .map(s -> s.replace('"', ' ').trim())
                    .collect(Collectors.toList());
            parkFields.put(collect.get(0), collect.get(1));

        }
        return parkFields;
    }
}
